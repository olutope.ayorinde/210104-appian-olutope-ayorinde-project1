<h1> Project 1 Progress</h1>

<h2> Servlet Classes:</h2>
<ul>
<li>EmployeeServlet- Processes GET requests for reimbursement request data and POST request for submitting a request</li>

<li>LoginServlet- Processes POST requests to authenticate user credentials.</li>

<li>ManagerServlet- Processes GET requests to view all pending requests and PUT requests to modify reimbursement request statuses.</li>
</ul>
<h3>**************************</h3>
<h2> Service Classes:</h2>
<ul>
<li>UserService- Uses the UsersDaos to to access login information.</li>

<li>ManagerService- Uses the RequestsDaos for viewing all pending requests and changing request statuses.</li>

<li>EmployeeService- Uses the RequestsDao to view pending/resolved requests and to create a request</li>
</ul>
<h3>**************************</h3>
<h2>Dao Classes:</h2>
<ul>
<li>UserDaos- Accesses user data from the database in order to login</li>

<li>RequestsDaos- Accesses reimbursement request data from the database for the manager and employee</li>
</ul>
<h3>**************************</h3>
<h2>Reimburse (Model) Classes:</h2>
<ul>
<li>Users- Contains User data and properties.</li>

<li>Requests- Contains request data and properties.</li>
</ul>
<h3>**************************</h3>
<h2>Database</h2>
<ul>
<li>Setup-script to to create the DB that holds Users and Requests tables to store data on the employees, managers, and the different requests</li>
<li> triiger and sequence script to auto-generate the user and request numbers when a new user or request is added to the database</li>

</ul>