package dev.ayo.reimburse;

public class Requests {
	
	private int requestNumber;
	private String userName;
	private String requestDate;
	private String description;
	private double requestAmount;
	private String requestStatus;
	
	public int getRequestNumber() {
		return requestNumber;
	}
	
	
	public void setRequestNumber(int requestNumber) {
		this.requestNumber = requestNumber;
	}
	
	
	public String getUserName() {
		return userName;
	}
	
	
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	
	public String getRequestDate() {
		return requestDate;
	}
	
	
	public void setRequestDate(String requestDate) {
		this.requestDate = requestDate;
	}
	
	
	public String getDescription() {
		return description;
	}
	
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	
	public double getRequestAmount() {
		return requestAmount;
	}
	
	
	public void setRequestAmount(double requestAmount) {
		this.requestAmount = requestAmount;
	}
	
	
	public String getRequestStatus() {
		return requestStatus;
	}
	
	
	public void setRequestStatus(String requestStatus) {
		this.requestStatus = requestStatus;
	}
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		long temp;
		temp = Double.doubleToLongBits(requestAmount);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((requestDate == null) ? 0 : requestDate.hashCode());
		result = prime * result + requestNumber;
		result = prime * result + ((requestStatus == null) ? 0 : requestStatus.hashCode());
		result = prime * result + ((userName == null) ? 0 : userName.hashCode());
		return result;
	}
	
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Requests other = (Requests) obj;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (Double.doubleToLongBits(requestAmount) != Double.doubleToLongBits(other.requestAmount))
			return false;
		if (requestDate == null) {
			if (other.requestDate != null)
				return false;
		} else if (!requestDate.equals(other.requestDate))
			return false;
		if (requestNumber != other.requestNumber)
			return false;
		if (requestStatus == null) {
			if (other.requestStatus != null)
				return false;
		} else if (!requestStatus.equals(other.requestStatus))
			return false;
		if (userName == null) {
			if (other.userName != null)
				return false;
		} else if (!userName.equals(other.userName))
			return false;
		return true;
	}
	
	
	@Override
	public String toString() {
		return "Requests [requestNumber=" + requestNumber + ", userName=" + userName + ", requestDate=" + requestDate
				+ ", description=" + description + ", requestAmount=" + requestAmount + ", requestStatus="
				+ requestStatus + "]";
	}

	
}
