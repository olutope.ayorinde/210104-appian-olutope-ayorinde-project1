package dev.ayo.Servlets;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import dev.ayo.Services.EmployeeService;
import dev.ayo.reimburse.Requests;
import dev.ayo.reimburse.Users;


public class EmployeeServlet {
	
	private EmployeeService empService =  new EmployeeService();
	private ObjectMapper om = new ObjectMapper();
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException{
		String type = request.getParameter("type");
		Users ur = om.readValue(request.getReader().readLine(), Users.class);
		List<Requests> requests = new ArrayList<>();
		if(type != null) {
			if (type.equals("pending")) {
				requests = empService.viewPending(ur.getUsername());
			} else if (type.equals("resolved")) {
				requests = empService.viewResolved(ur.getUsername());
			}
			
			if (requests != null) {
				String jsonRequests = om.writeValueAsString(requests);
				PrintWriter pw = response.getWriter();
				pw.write(jsonRequests);
				pw.close();
			} else {
				response.sendError(400);
			}
		
		} else {
			response.sendError(400);
		}
	}
	
	
	public void doPost(HttpServletRequest request, HttpServletResponse response) 
			throws IOException, ServletException{
		Requests req = om.readValue(request.getReader().readLine(), Requests.class);
		empService.createRequest(req.getUserName(), req.getRequestDate(), req.getDescription(), req.getRequestAmount());
	}
}
