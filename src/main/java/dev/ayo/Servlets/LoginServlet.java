package dev.ayo.Servlets;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import dev.ayo.Services.UserService;
import dev.ayo.reimburse.Users;



public class LoginServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	private UserService userService = new UserService();
	private ObjectMapper om = new ObjectMapper();
	
	public void doPost(HttpServletRequest request, HttpServletResponse response) 
			throws IOException, ServletException{
		
		Users uCredentials = om.readValue(request.getReader().readLine(), Users.class);
		Users newUser = userService.userLogin(uCredentials.getUsername(), uCredentials.getPassword());
		
		if(newUser.getUserNumber() != 0) {
			System.out.println("success");
		}else {
			response.sendError(401);
		}
	}
	

}
