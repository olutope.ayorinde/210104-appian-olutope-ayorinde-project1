package dev.ayo.Servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import dev.ayo.reimburse.Requests;
import dev.ayo.Services.ManagerService;

public class ManagerServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	private ManagerService manService = new ManagerService();
	private ObjectMapper om = new ObjectMapper();
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException{
		
		List<Requests> pendingRequests = new ArrayList<>();
		pendingRequests= manService.viewAllPending();
		System.out.println("Get request to project 1 manager Servlet");
		
		if (pendingRequests != null) {
			String jsonRequests = om.writeValueAsString(pendingRequests);
			System.out.println(jsonRequests);
			PrintWriter pw = response.getWriter();
			pw.write(jsonRequests);
			pw.close();
		} else {
			response.sendError(400);
		}
	}
	
	
	public void doPut(HttpServletRequest request, HttpServletResponse response) 
			throws IOException, ServletException {
		String type = request.getParameter("type");
		Requests penRequest = om.readValue(request.getReader().readLine(), Requests.class);
		if((type != null) && (penRequest != null)) {
			if(type.equals("accept")) {
				this.manService.acceptRequests(penRequest.getUserName(), penRequest.getRequestNumber());
			} else if (type.equals("reject")) {
				this.manService.rejectRequests(penRequest.getUserName(), penRequest.getRequestNumber());
			}
		} else {
			response.sendError(400);
		}
	}

}
