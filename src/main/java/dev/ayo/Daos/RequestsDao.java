package dev.ayo.Daos;

import java.util.List;

import dev.ayo.reimburse.Requests;

public interface RequestsDao {
	
	public List<Requests> getPendingRequests(String username);
	public List<Requests> getResolvedRequests(String username);
	public List<Requests>getAllPendingRequests();
	public boolean rejectRequest(String username, int requestNumber);
	public boolean acceptRequest(String username, int requestNumber);
	public boolean createRequest(String username, String requestDate, String description, double requestAmount);
}
