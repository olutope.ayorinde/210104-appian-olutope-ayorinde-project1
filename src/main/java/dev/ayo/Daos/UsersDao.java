package dev.ayo.Daos;

import dev.ayo.reimburse.Users;

public interface UsersDao {

	public Users userLogin(String username, String password);
}
