package dev.ayo.Daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dev.ayo.Util.ConnectionUtil;
import dev.ayo.reimburse.Users;

public class UserDaosImpl implements UsersDao {

	@Override
	public Users userLogin(String username, String password) {
		try {
			String sql = "SELECT * FROM JOB_USERS WHERE USERNAME = ? AND PASSWORD = ?" ;
			Connection connection = ConnectionUtil.getConnection();
			PreparedStatement pStatement = connection.prepareStatement(sql);
			pStatement.setString(1, username);
			pStatement.setString(2, password);
			ResultSet resultSet = pStatement.executeQuery();
			Users user = new Users();
			while(resultSet.next()) {
				user.setUserNumber(resultSet.getInt("USER_NUMBER"));
				user.setUsername(resultSet.getString("USERNAME"));
				user.setPassword(resultSet.getString("PASSWORD"));
				user.setManagerID(resultSet.getInt("MANAGER_ID"));
			}
			return user;	
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		return null;
	}

}
