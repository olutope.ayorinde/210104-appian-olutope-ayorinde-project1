package dev.ayo.Daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.ArrayList;

import dev.ayo.reimburse.Requests;
import dev.ayo.Util.ConnectionUtil;


public class RequestDaosImpl implements RequestsDao {

	@Override
	public List<Requests> getPendingRequests(String username)  {
		try {
			String sql = "SELECT * FROM REQUESTS WHERE USERNAME = ? AND REQUEST_STATUS = 'PENDING'";
			Connection connection = ConnectionUtil.getConnection();
			PreparedStatement pStatement = connection.prepareStatement(sql);
			pStatement.setString(1, username);
			ResultSet resultSet = pStatement.executeQuery();
			List<Requests> requests = new ArrayList<>();
			Requests request = new Requests(); 
			while(resultSet.next()) {
				request.setRequestNumber(resultSet.getInt("REQUEST_NUMBER"));
				request.setUserName(resultSet.getString("USERNAME"));
				request.setRequestDate(resultSet.getString("REQUEST_DATE"));
				request.setDescription(resultSet.getString("DESCRIPTION"));
				request.setRequestAmount(resultSet.getDouble("REQUEST_AMOUNT"));
				request.setRequestStatus(resultSet.getString("REQUEST_STATUS"));
				requests.add(request);
			}
			return requests;	
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<Requests> getResolvedRequests(String username) {
		try {
			String sql = "SELECT * FROM REQUESTS WHERE USERNAME = ? AND REQUEST_STATUS != 'PENDING'";
			Connection connection = ConnectionUtil.getConnection();
			PreparedStatement pStatement = connection.prepareStatement(sql);
			pStatement.setString(1, username);
			ResultSet resultSet = pStatement.executeQuery();
			List<Requests> requests = new ArrayList<>();
			Requests request = new Requests(); 
			while(resultSet.next()) {
				request.setRequestNumber(resultSet.getInt("REQUEST_NUMBER"));
				request.setUserName(resultSet.getString("USERNAME"));
				request.setRequestDate(resultSet.getString("REQUEST_DATE"));
				request.setDescription(resultSet.getString("DESCRIPTION"));
				request.setRequestAmount(resultSet.getDouble("REQUEST_AMOUNT"));
				request.setRequestStatus(resultSet.getString("REQUEST_STATUS"));
				requests.add(request);
			}
			return requests;	
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<Requests> getAllPendingRequests() {
		try {
			Connection connection = ConnectionUtil.getConnection();
			Statement Statement = connection.createStatement();
			ResultSet resultSet = Statement.executeQuery("SELECT * FROM REQUESTS WHERE REQUEST_STATUS = 'PENDING'");
			List<Requests> requests = new ArrayList<>();
			Requests request = new Requests(); 
			while(resultSet.next()) {
				request.setRequestNumber(resultSet.getInt("REQUEST_NUMBER"));
				request.setUserName(resultSet.getString("USERNAME"));
				request.setRequestDate(resultSet.getString("REQUEST_DATE"));
				request.setDescription(resultSet.getString("DESCRIPTION"));
				request.setRequestAmount(resultSet.getDouble("REQUEST_AMOUNT"));
				request.setRequestStatus(resultSet.getString("REQUEST_STATUS"));
				requests.add(request);
			}
			return requests;	
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		return null;
	}
	 
	@Override
	public boolean rejectRequest(String username, int requestNumber) {
		String sql = "UPDATE REQUESTS SET REQUEST_STATUS = 'REJECTED' WHERE USERNAME = ? AND REQUEST_NUMBER = ? ";
		int numOfItemsUpdated = 0;
		try {
			Connection connection = ConnectionUtil.getConnection();
			PreparedStatement pStatement = connection.prepareStatement(sql);
			pStatement.setString(1, username);
			pStatement.setInt(2, requestNumber);
			numOfItemsUpdated = pStatement.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		if(numOfItemsUpdated==1) {
			return true;
		}
		return false;
	}

	@Override
	public boolean acceptRequest(String username, int requestNumber) {
		String sql = "UPDATE REQUESTS SET REQUEST_STATUS = 'ACCEPTED' WHERE USERNAME = ? AND REQUEST_NUMBER = ? ";
		int numOfItemsUpdated = 0;
		try {
			Connection connection = ConnectionUtil.getConnection();
			PreparedStatement pStatement = connection.prepareStatement(sql);
			pStatement.setString(1, username);
			pStatement.setInt(2, requestNumber);
			numOfItemsUpdated = pStatement.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		if(numOfItemsUpdated==1) {
			return true;
		}
		return false;
	}

	@Override
	public boolean createRequest(String username, String requestDate, String description, double requestAmount) {
		String sql = "INSERT INTO REQUESTS (USERNAME, REQUEST_DATE, DESCRIPTION, REQUEST_AMOUNT) VALUES (?, ?, ?, ?)";
		int numOfItemsUpdated = 0;
		try {		
			Connection connection = ConnectionUtil.getConnection();
			PreparedStatement pStatement = connection.prepareStatement(sql);
			pStatement.setString(1, username);
			pStatement.setString(2, requestDate);
			pStatement.setString(3, description);
			pStatement.setDouble(4, requestAmount);
			numOfItemsUpdated = pStatement.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		if(numOfItemsUpdated==1) {
			return true;
		}
		return false;
	}

}
