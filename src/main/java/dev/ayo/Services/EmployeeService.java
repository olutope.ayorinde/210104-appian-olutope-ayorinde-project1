package dev.ayo.Services;

import java.util.List;

import dev.ayo.Daos.RequestDaosImpl;
import dev.ayo.Daos.RequestsDao;
import dev.ayo.reimburse.Requests;

public class EmployeeService {
	
	private RequestsDao erDao = new RequestDaosImpl();
	
	public List<Requests> viewPending(String username){
		return erDao.getPendingRequests(username);
	}

	public List<Requests> viewResolved(String username){
		return erDao.getResolvedRequests(username);
	}
	
	public boolean createRequest(String username, String requestDate, String description, double requestAmount) {
		return erDao.createRequest(username, requestDate, description, requestAmount);
	}
}
