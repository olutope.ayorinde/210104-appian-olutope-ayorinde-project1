package dev.ayo.Services;

import dev.ayo.Daos.UserDaosImpl;
import dev.ayo.Daos.UsersDao;
import dev.ayo.reimburse.Users;

public class UserService {
	
	private UsersDao user = new UserDaosImpl();
	
	public Users userLogin (String username, String password) {
		
		return user.userLogin(username, password);
	}

}
