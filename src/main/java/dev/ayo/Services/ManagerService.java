package dev.ayo.Services;

import java.util.List;

import dev.ayo.Daos.RequestDaosImpl;
import dev.ayo.Daos.RequestsDao;
import dev.ayo.reimburse.Requests;

public class ManagerService {
	
	private RequestsDao mrDao = new RequestDaosImpl();
	
	public List<Requests> viewAllPending(){
		return mrDao.getAllPendingRequests();
	}
	
	public boolean acceptRequests(String username, int requestNumber ) {
		return mrDao.acceptRequest(username, requestNumber);
	}
	
	public boolean rejectRequests(String username, int requestNumber ) {
		return mrDao.rejectRequest(username, requestNumber);
	}

}
